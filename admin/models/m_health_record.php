<?php
require_once ("database.php");
class m_health_record extends database {
    public function read_health_record_with_name_customer() {
        $sql = "select hs.*,ho_ten from ho_so_suc_khoe hs,khach_hang kh where hs.id_khach_hang=kh.id";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function search_health_record_customer($ho_ten,$ngay_kham) {
        $sql = "select hs.*,kh.ho_ten from ho_so_suc_khoe hs,khach_hang kh where hs.id_khach_hang=kh.id AND kh.ho_ten LIKE '%$ho_ten%' AND hs.ngay_kham LIKE '%$ngay_kham%'";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function edit_health($huyet_ap,$nhip_tim,$nhiet_do,$chieu_cao,$can_nang,$nhom_mau,$thong_tin_benh,$ngay_kham,$trang_thai,$id_khach_hang) {
        $sql = "update ho_so_suc_khoe set huyet_ap = ?, nhip_tim = ?,nhiet_do = ?,chieu_cao = ?,can_nang = ?,nhom_mau = ?,thong_tin_benh = ?,ngay_kham = ?,trang_thai = ? where id_khach_hang = ?";
        $this->setQuery($sql);
        return $this->execute(array($huyet_ap,$nhip_tim,$nhiet_do,$chieu_cao,$can_nang,$nhom_mau,$thong_tin_benh,$ngay_kham,$trang_thai,$id_khach_hang));
    }
}
?>