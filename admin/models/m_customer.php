<?php
include_once ("database.php");
class m_customer extends database {
    public function read_customer()
    {
        $sql = "select * from khach_hang ";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function read_customer_by_id($id) {
        $sql = "select * from khach_hang where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }

    public function read_id($id) {
        $sql = "select * from ho_so_suc_khoe where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }

    public function read_health_record_with_name_customer($id) {
        $sql = "select hs.*,kh.ho_ten,kh.id,kh.id_nguoi_dung from ho_so_suc_khoe hs,khach_hang kh where hs.id_khach_hang=kh.id and kh.id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }

    public function log_move($id_khach_hang) {
        $sql = "select * from log_khai_bao_y_te where id_khach_hang = ? ORDER BY ngay_khoi_hanh DESC;";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id_khach_hang));
    }

    public function count_user() {
        $sql = "select count(*) as CT from khach_hang";
        $this->setQuery($sql);
        return $this->loadRow();
    }

    public function count_registered() {
        $sql = "select count(*) as CT from dang_ky_tiem";
        $this->setQuery($sql);
        return $this->loadRow();
    }

    public function count_file() {
        $sql = "select count(*) as CT from ho_so_suc_khoe";
        $this->setQuery($sql);
        return $this->loadRow();
    }

    public function search_customer($ho_ten,$so_dien_thoai,$nghe_nghiep,$ngay_sinh,$dia_chi) {
        $sql = "select * from khach_hang where ho_ten LIKE '%$ho_ten%' AND so_dien_thoai LIKE '%$so_dien_thoai%' AND nghe_nghiep LIKE '%$nghe_nghiep%' AND ngay_sinh LIKE '%$ngay_sinh%' AND dia_chi LIKE '%$dia_chi%'";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
}
?>