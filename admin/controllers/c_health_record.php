<?php
include_once ("models/m_health_record.php");
include_once ("models/m_customer.php");
class c_health_record {
    public function show_all_health_record()
    {
        $m_health_record = new m_health_record();
            if (isset($_POST['btnsearch'])) {
                $ho_ten = $_POST['ho_ten'];
                $ngay_kham = $_POST['ngay_kham'];
                $health_record = $m_health_record->search_health_record_customer($ho_ten,$ngay_kham);
            } else {
                $health_record = $m_health_record->read_health_record_with_name_customer();
            }

        $view = "views/health_record/v_health_record.php";
        include("templates/font-end/layout.php");
    }

    public function update_health_record()
    {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $m_health_customer = new m_customer();
            $m_record = new m_health_record();
            $id_customer = $m_health_customer->read_id($id);
            $id_nd = $id_customer->id_khach_hang;
            $health_record_customer = $m_health_customer->read_health_record_with_name_customer($id_nd);
            $error = [];
            if (isset($_POST['btnsubmit'])) {
                $chieu_cao = $_POST['chieu_cao'];
                $can_nang = $_POST['can_nang'];
                $huyet_ap = $_POST['huyet_ap'];
                $nhip_tim = $_POST['nhip_tim'];
                $nhiet_do = $_POST['nhiet_do'];
                $nhom_mau = $_POST['nhom_mau'];
                $ngay_kham = $_POST['ngay_kham'];
                $thong_tin_benh = $_POST['thong_tin_benh'];
                $trang_thai = 1;
                $result = $m_record->edit_health($huyet_ap,$nhip_tim,$nhiet_do,$chieu_cao,$can_nang,$nhom_mau,$thong_tin_benh,$ngay_kham,$trang_thai,$id_nd);
                if ($result) {
                    $done[] = "Cập nhật thông tin thành công!!!";
                } else {
                    $error[] = "Cập nhật thông tin không thành công!!!";
                }
            }
        }
        $view = "views/edit_health_record/v_edit_health_record.php";
        include("templates/font-end/layout.php");
    }

}
