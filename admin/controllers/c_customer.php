<?php
include_once ("models/m_customer.php");
class c_customer {
    public function show_all_customer() {
        $m_customer = new m_customer();
        if (isset($_POST['btnsearch'])) {
            $ho_ten =$_POST['ho_ten'];
            $so_dien_thoai = $_POST['so_dien_thoai'];
            $nghe_nghiep = $_POST['nghe_nghiep'];
            $ngay_sinh = $_POST['ngay_sinh'];
            $dia_chi = $_POST['dia_chi'];
            $customer = $m_customer->search_customer($ho_ten,$so_dien_thoai,$nghe_nghiep,$ngay_sinh,$dia_chi);
        } else {
            $customer = $m_customer->read_customer();
        }

        $view = "views/customer/v_customer.php";
        include ("templates/font-end/layout.php");
    }

    public function show_details_customer() {
        if(isset($_GET['id'])) {
            $id_customer = $_GET['id'];
            $m_detail_customer = new m_customer();
            $customer = $m_detail_customer->read_customer_by_id($id_customer);
            $detail_customer = $m_detail_customer->read_customer_by_id($id_customer);
            $health_record_customer = $m_detail_customer->read_health_record_with_name_customer($id_customer);
            $id_kh = $detail_customer->id_nguoi_dung;
            $move = $m_detail_customer->log_move($id_kh);
        }
        $view = "views/customer_details/v_customer_details.php";
        include ("templates/font-end/layout.php");
    }

}
?>