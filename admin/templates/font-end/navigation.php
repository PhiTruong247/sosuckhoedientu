<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar position-relative">
        <div class="help-bt">
            <a href="tel:108" class="d-flex align-items-center">
                <div class="bg-danger rounded10 h-50 w-50 l-h-50 text-center me-15">
                    <i data-feather="mic"></i>
                </div>
                <h4 class="mb-0">Emergency<br>help</h4>
            </a>
        </div>
        <div class="multinav">
            <div class="multinav-scroll" style="height: 100%;">
                <!-- sidebar menu-->
                <ul class="sidebar-menu pb-200" data-widget="tree">
                    <li>
                        <a href="index.php">
                            <i data-feather="monitor"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="customer.php">
                            <i data-feather="inbox"></i>
                            <span>Quản lý khách hàng</span>
                        </a>
                    </li>
                    <li>
                        <a href="health_record.php">
                            <i data-feather="users"></i>
                            <span>Quản lý hồ sơ sức khỏe</span>
                        </a>
                    </li>
                    <li>
                        <a href="vaccination_infor.php">
                            <i data-feather="activity"></i>
                            <span>Quản lý tiêm chủng</span>
                        </a>
                    </li>
                </ul>
                <div class="sidebar-widgets">
                    <div class="mx-25 mb-30 pb-20 side-bx bg-primary-light rounded20">
                        <div class="text-center">
                            <img src="https://rhythm-admin-template.multipurposethemes.com/images/svg-icon/color-svg/custom-17.svg" class="sideimg p-5" alt="">
                            <h4 class="title-bx text-primary">Make an Appointments</h4>
                            <a href="#" class="py-10 fs-14 mb-0 text-primary">
                                Best Helth Care here <i class="mdi mdi-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="copyright text-center m-25">
                        <p><strong class="d-block">Sổ sức khỏe điện tử</strong> © <script>document.write(new Date().getFullYear())</script> All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</aside>
