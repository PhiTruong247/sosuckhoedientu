<?php
include_once ("database.php");
class m_registered_vaccin extends database {

    public function read_email_user($email) {
        $sql = "select * from khach_hang where email = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($email));
    }

    public function read_province() {
        $sql = "select * from tinh_thanh_pho";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function add_registered($id,$id_khach_hang,$ho_ten,$ngay_sinh,$gioi_tinh,$can_cuoc_cong_dan,$so_the_bao_hiem,$thoi_gian_mong_muon,$nghe_nghiep,$so_dien_thoai,$dia_chi,$nguoi_tao) {
        $sql = "insert into dang_ky_tiem (id,id_khach_hang,ho_ten,ngay_sinh,gioi_tinh,can_cuoc_cong_dan,so_the_bao_hiem,thoi_gian_mong_muon_tiem,nghe_nghiep,so_dien_thoai,dia_chi,nguoi_tao) values(?,?,?,?,?,?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$id_khach_hang,$ho_ten,$ngay_sinh,$gioi_tinh,$can_cuoc_cong_dan,$so_the_bao_hiem,$thoi_gian_mong_muon,$nghe_nghiep,$so_dien_thoai,$dia_chi,$nguoi_tao));
    }

    public function question_registered($id,$id_dang_ky,$cau_1,$cau_2,$cau_3,$cau_4,$cau_5,$cau_6,$cau_7,$cau_8,$cau_9,$cau_10,$ngay_tao) {
        $sql = "insert into cau_hoi_dang_ky_tiem values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$id_dang_ky,$cau_1,$cau_2,$cau_3,$cau_4,$cau_5,$cau_6,$cau_7,$cau_8,$cau_9,$cau_10,$ngay_tao));
    }

    public function show_province($id) {
        $sql = "select ten_tinh_thanh_pho from tinh_thanh_pho where id = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }

    public function show_dictricts($id) {
        $sql = "select ten_quan_huyen from quan_huyen where id = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }

    public function show_wards($id) {
        $sql = "select ten_xa_phuong from xa_phuong where id = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }
}
?>