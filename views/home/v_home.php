<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="media align-items-center mb-20 bg-primary">
                        <?php
                        if ($read_email->hinh_anh == "") { ?>
                            <img src="public/layout/images/avatar/2.jpg" class="avatars" alt="" />
                        <?php } else { ?>
                            <img src="public/layout/imageinfo/<?php echo $read_email->hinh_anh ?>" class="avatars" alt="" />
                            <?php
                        }
                        ?>
                        <div class="media-body">
                            <p class="text-white"><strong>THẺ THÔNG TIN COVID</strong></p>
                            <p class="text-white">Họ & Tên: <?php echo $read_email->ho_ten;?></p>
                            <p class="text-white">Giới tính: <?php if ($read_email->gioi_tinh == 0) {echo "Nam";} else {echo "Nữ";}?> / <?php echo date("d-m-Y", strtotime($read_email->ngay_sinh));?></p>
                            <p class="text-white">Trạng thái: <?php if ($read_email->trang_thai == 0) {echo "chưa tiêm";} else {echo "Đã tiêm chủng";}?></p>
                        </div>
                        <div>
                            <button type="submit" id="chi_tiet" onclick="window.location.href='patient-details.php'" class="btn btn-outline btn-white btn-sm btn-rounded">Chi tiết</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-12">
                    <a class="box box-link-shadow text-center" href="information-adress.php">
                        <div class="box-body">
                            <div class="fs-22">Khai báo y tế</div>
                        </div>
                        <div class="box-body bg-info btsr-0 bter-0">
                            <p>
                                <i class="fad fa-file-alt fs-40"></i>
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-12">
                    <a class="box box-link-shadow text-center" href="patient-details.php">
                        <div class="box-body">
                            <div class="fs-22">Thông tin người dùng</div>
                        </div>
                        <div class="box-body bg-success btsr-0 bter-0">
                            <p>
                                <i class="fad fa-file-medical-alt fs-40"></i>
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-12">
                    <a class="box box-link-shadow text-center" href="registered-vaccin.php">
                        <div class="box-body">
                            <div class="fs-22">Đăng ký tiêm</div>
                        </div>
                        <div class="box-body bg-warning btsr-0 bter-0">
                            <p>
                                <i class="fad fa-info-square fs-40"></i>
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-12">
                    <a class="box box-link-shadow text-center" href="javascript:void(0)">
                        <div class="box-body">
                            <div class="fs-22">Hotline 115</div>
                        </div>
                        <div class="box-body bg-danger btsr-0 bter-0">
                            <p>
                                <i class="fad fa-phone-square fs-40"></i>
                            </p>
                        </div>
                    </a>
                </div>
            </div>

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">Thư viện Ảnh</h4>
                </div>
                <div class="box-body">
                    <div class="zoom-gallery m-t-30">
                        <a href="public/layout/images/gallery/thumb/7.jpg" title="Caption. Can be aligned to any side and contain any HTML.">
                            <img src="public/layout/images/gallery/thumb/7.jpg" width="32.5%" alt="" />
                        </a>
                        <a href="public/layout/images/gallery/thumb/8.jpg" title="This image fits only horizontally.">
                            <img src="public/layout/images/gallery/thumb/8.jpg" width="32.5%" alt="" />
                        </a>
                        <a href="public/layout/images/gallery/thumb/9.jpg">
                            <img src="public/layout/images/gallery/thumb/9.jpg" width="32.5%" alt="" />
                        </a>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- Default box -->

            <div class="col-12">
                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h4 class="box-title">Lịch trình di chuyển</h4>
                    </div>
                    <div class="box-body">
                        <div id="slimtest5">
                            <div class="box-body">
                                <div class="timeline-line timeline-line-dotted">
                                    <?php foreach ($move as $value) { ?>
                                        <div class="timeline__group">
                                            <div class="timeline__box">
                                                <div class="timeline__date">
                                                    <span class="timeline__month"><?php echo date("d-m-Y", strtotime($value->ngay_khoi_hanh))?></span>
                                                </div>
                                                <div class="timeline__post">
                                                    <div class="timeline__content">
                                                        <div class="timeline-heading">
                                                            <h4 class="timeline-title"><?php echo $value->phuong_tien . " - " . $value->so_hieu_phuong_tien; ?></h4>
                                                        </div>
                                                        <div class="timeline-body">
                                                            <p>Điểm đi: <?php echo $value->diem_di;?></p>
                                                        </div>
                                                        <div class="timeline-body">
                                                            <p>Điểm đến: <?php echo $value->diem_den;?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

            <div class="col-12">
                <div class="box">
                    <div class="box-header">
                        <h4 class="box-title">Cẩm nang y tế</h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="owl-carousel owl-theme">
                            <div class="box mb-0">
                                <img class="card-img-top img-responsive" src="public/layout/images/card/img1.jpg" alt="Card image cap">
                            </div>
                            <div class="box mb-0">
                                <img class="card-img-top img-responsive" src="public/layout/images/card/img2.jpg" alt="Card image cap">
                            </div>
                            <div class="box mb-0">
                                <img class="card-img-top img-responsive" src="public/layout/images/card/img3.jpg" alt="Card image cap">
                            </div>
                            <div class="box mb-0">
                                <img class="card-img-top img-responsive" src="public/layout/images/card/img4.jpg" alt="Card image cap">
                            </div>
                            <div class="box mb-0">
                                <img class="card-img-top img-responsive" src="public/layout/images/card/img1.jpg" alt="Card image cap">
                            </div>
                            <div class="box mb-0">
                                <img class="card-img-top img-responsive" src="public/layout/images/card/img2.jpg" alt="Card image cap">
                            </div>

                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->